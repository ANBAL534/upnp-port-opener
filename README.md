## UPNP Port Oppener Helper
A super simple script that thelps to open temporal ports in upnp-enabled routers without the need to get into your router's configuration.

The main idea behind this script was to send this script to a friend that has no familiarity with computers but wants to host a videogame server for a quick game with their friends, or to embed this script in your programs to add upnp functionality.
import com.dosse.upnp.UPnP
import kotlin.system.exitProcess

fun main (args: Array<String>) {
    if ("-h" in args || ("--tcp" !in args && "--udp" !in args && "--both" !in args)) {
        println("""
            Usage: java -jar upnp-port-opener.jar [options]
                Options:
                    * -h              Shows this message
                    * --tcp [port]    Opens tcp [port] in the router for this machine, must be an integer
                    * --udp [port]    Opens udp [port] in the router for this machine, must be an integer
                    * --both [port]   Opens tcp and udp [port] in the router for this machine, must be an integer
                
                Example:
                    java -jar upnp-port-opener.jar --tcp 32445 --udp 45765 --tcp 34122 --both 9834
        """.trimIndent())
        exitProcess(0)
    }

    for (arg in args) {
        try {
            val port = arg.toInt()
            val indexOf = args.indexOf(arg)
            val type = args[indexOf-1]

            when (type) {
                "tcp" -> UPnP.openPortTCP(port)
                "udp" -> UPnP.openPortUDP(port)
                "both" -> {
                    UPnP.openPortTCP(port)
                    UPnP.openPortUDP(port)
                }
            }

            println("${type.capitalize()} $port port opened.")
        } catch (e: NumberFormatException) {
            // Ignored
        }
    }

    println("If your router supports UPnP the above mentioned ports should be opened and pointing towards this machine.")
}